package assignment;

import java.sql.*;
import java.util.*;

public class DatabaseUpdateOperation 
{

	Scanner sc=new Scanner(System.in);
	int choice;
	String ch;
	String query="update student_assign set ";
	public static void main(String[] args) 
	{
		DatabaseUpdateOperation du=new DatabaseUpdateOperation();
		String url="jdbc:oracle:thin:@localhost:1521:orcl";
		String username="hr";
		String password="hr";
		du.selectCol();
		System.out.println("Query: "+du.query);
		try
		{
			Connection con=DriverManager.getConnection(url, username, password);
			Statement c=con.createStatement();
			int x=c.executeUpdate(du.query);
			System.out.println(x+" row updated");
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public void selectCol()
	{
		
		System.out.println("1:Name\n2:Age\n3:Address\n4:Course Id\nEnter your choice to update");
		choice=sc.nextInt();
		switch(choice)
		{
			case 1:System.out.println("Enter name of the student to update");
			       String name=sc.next();
			       query+="name='"+name+"' where ";
				   System.out.println("On basis of which column you want to update\nAge\nAddress\nCourse Id\nEnter your choice");
				   ch=sc.next();
				   updateVal();
				   break;
			case 2:System.out.println("Enter age of the student to update");
			       int age=sc.nextInt();
			       query+="age="+age+" where ";
				   System.out.println("On basis of which column you want to update\nName\nAddress\nCourse Id\nEnter your choice");
			   	   ch=sc.next();
			   	   updateVal();
			   	   break;
			case 3:System.out.println("Enter address of the student to update");
			       String address=sc.next();
			       query+="address='"+address+"' where ";
				   System.out.println("On basis of which column you want to update\nName\nAge\nCourse Id\nEnter your choice");
		   	       ch=sc.next();
		   	       updateVal();
		   	       break;
			case 4:System.out.println("Enter course id of the student to update");
				   int cou_id=sc.nextInt();
				   query+="course_id="+cou_id+" where ";
				   System.out.println("On basis of which column you want to update\nName\nAddress\nCourse Id\nEnter your choice");
		   	       ch=sc.next();
		   	       updateVal();
		   	       break;
		   default:System.out.println("Invalid Choice");
		}
	}
	
	public void updateVal()
	{
		switch(ch)
		{
			case "Name":System.out.println("Enter name for reference");
   			            String name=sc.next();
			            query+="name='"+name+"'";
			            break;
			case "Age":System.out.println("Enter age for reference");
			           int age=sc.nextInt();
			           query+="age="+age;
			           break;
			case "Address":System.out.println("Enter address for reference");
	           			   String address=sc.next();
	                       query+="address='"+address+"'";
				           break;
			case "Course Id":System.out.println("Enter Course id for reference");
	           				 String course=sc.next();
	           				 query+="course_id="+course;
	           				 break;
	       default: System.out.println("Invalid Choice");
		   
		}
	}

}
